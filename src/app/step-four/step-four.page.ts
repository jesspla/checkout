import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Order } from '../models';
import { StepService } from '../services/step.service';

@Component({
  selector: 'app-step-four',
  templateUrl: './step-four.page.html',
  styleUrls: ['./step-four.page.scss'],
})
export class StepFourPage implements OnInit {

  order: Order

  constructor(private readonly stepService: StepService, private router: Router) {
    this.order = this.router.getCurrentNavigation().extras.state as Order
  }

  ngOnInit() {
  }


  ionViewWillEnter() {
    this.stepService.updateCurrentStepState(4)

  }



}
