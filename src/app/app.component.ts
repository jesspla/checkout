import { Component, OnInit } from '@angular/core';
import { StepService } from './services/step.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  currentStep: number = 0

  constructor(private readonly stepService: StepService) {}
  
  ngOnInit(): void {
    this.stepService.currentStep.subscribe((value) => {
      this.currentStep = value
    })
  }

  isActive(step: number): boolean {
    return step === this.currentStep
  }

  isFinished(step: number): boolean {
    return step < this.currentStep
  }
}
