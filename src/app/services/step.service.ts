import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({providedIn: 'root'})
export class StepService {
    private readonly currentStepState = new Subject<number>()

    currentStep: Observable<number>

    constructor() {
        this.currentStep = this.currentStepState.asObservable()
    }

    updateCurrentStepState(newCurrentStep?: number): void {
        this.currentStepState.next(newCurrentStep)
    }
    
}