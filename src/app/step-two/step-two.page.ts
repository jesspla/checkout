import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Order } from '../models';
import { StepService } from '../services/step.service';

@Component({
  selector: 'app-step-two',
  templateUrl: './step-two.page.html',
  styleUrls: ['./step-two.page.scss'],
})
export class StepTwoPage implements OnInit {
  order: Order
  addressForm: FormGroup

  constructor(private readonly fb: FormBuilder, private readonly stepService: StepService, private router: Router, readonly navCtrl: NavController) {
    this.order = this.router.getCurrentNavigation().extras.state as Order
  }

  ngOnInit(): void {
    this.addressForm = this.fb.group({
      address: ['', [Validators.required]],
      country: ['', [Validators.required]],
      provincia: ['', [Validators.required]],
      city: ['', [Validators.required]],
      postalCode: ['', [Validators.required]]
    })
  }
  
  ionViewWillEnter() {
    this.stepService.updateCurrentStepState(2)

  }


  goToStepThree() {

    this.order = {
      ...this.order,
      address: this.addressForm.get("address").value,
      country: this.addressForm.get("country").value,
      provincia: this.addressForm.get("provincia").value,
      city: this.addressForm.get("city").value,
      postalCode: this.addressForm.get("postalCode").value,
    } as Order

    this.router.navigate(["/step-three"], { state: this.order })
  }

}
