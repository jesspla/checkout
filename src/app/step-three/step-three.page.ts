import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Order } from '../models';
import { StepService } from '../services/step.service';

@Component({
  selector: 'app-step-three',
  templateUrl: './step-three.page.html',
  styleUrls: ['./step-three.page.scss'],
})
export class StepThreePage implements OnInit {

  order: Order

  constructor(private readonly stepService: StepService, private router: Router, readonly navCtrl: NavController) {
    this.order = this.router.getCurrentNavigation().extras.state as Order

   }
  
  ngOnInit() {
  }


  ionViewWillEnter() {
    this.stepService.updateCurrentStepState(3)

  }

  goToStepFour() {

    this.order = {
      ...this.order
    } as Order

    this.router.navigate(["/step-four"], { state: this.order })
  }


}
