import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Order } from '../models';
import { StepService } from '../services/step.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  userDates: FormGroup
  order: Order = {} as Order

  constructor(private readonly fb: FormBuilder, private readonly stepService: StepService, private readonly router: Router) {}

  ngOnInit(): void {
    this.userDates = this.fb.group({
      name: ['', [Validators.required]],
      surname: ['', [Validators.required]],
      birthday: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required]]
    })
  }

  ionViewWillEnter() {
    this.stepService.updateCurrentStepState(1)
  }

  goToStepTwo() {

    this.order = {
      user: {
        name: this.userDates.get("name").value,
        surname: this.userDates.get("surname").value,
        birthday: this.userDates.get("birthday").value,
        email: this.userDates.get("email").value,
        phone: this.userDates.get("phone").value
      }
    } as Order

    this.router.navigate(["/step-two"], { state: this.order })
  }

}
