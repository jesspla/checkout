import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StepTwoPageRoutingModule } from './step-two-routing.module';

import { StepTwoPage } from './step-two.page';
import { StepService } from '../services/step.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    StepTwoPageRoutingModule
  ],
  declarations: [StepTwoPage],
})
export class StepTwoPageModule {}
