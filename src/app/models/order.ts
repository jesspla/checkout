import { User } from ".";

export interface Order {
    user: User,
    address: string
    country: string
    provincia: string
    city: string
    postalCode: number
}