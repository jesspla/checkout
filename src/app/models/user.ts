import { EmailValidator } from "@angular/forms";

export interface User {
    name: string
    surname: string
    birthday: Date
    email: string
    phone: number
}